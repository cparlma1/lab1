#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>
#include <time.h>
#include <string.h>
#include "timething.h"
#include <sys/time.h>


int compare(const void *p1, const void *p2){
	return (*(int*)p1 - *(int*)p2);
}

int main(int argc, char *argv[]){
	srand(time(NULL));
	
	short uflag=0;
	short gflag=0;
	short nflag=0;
	short mflag=0;
	short Mflag=0;
	short sflag=0;
	short iflag=0;
	short oflag=0;
	short cflag=0;
	int flag =0;
	char *flagvalue = NULL;
	opterr=0;
	int min = 1;
	int max = 256;
	int expected_num = 100;
	int numInts = 100;
	unsigned random = 0;
	char *filein = NULL;
	char *fileout = NULL;
	char *countfilename = NULL;
	FILE *input;
	FILE *output;
	FILE *countout;
	int * input_numbers = NULL;
	int *match_count = NULL;
	int out_of_range = 0;
	char *user = getenv("USER");


	//-u print usage string on stderr and exit	
	//-g put program in generate mode
	while(( flag = getopt (argc, argv, "ugn:m:M:s:i:o:c:")) != -1)
		switch (flag)
		{
			case 'u':
				uflag = 1;
				break;
			case 'g':
				gflag = 1;
				break;
			case 'n':
				nflag = 1;
				numInts = atoi(optarg);
				break;
			case 'm':
				mflag = 1;
				min = atoi(optarg);
				break;
			case 'M':
				Mflag = 1;
				max = atoi(optarg);
				break;
			case 's':
				sflag = 1;
				//int seed = atoi(optarg);
				srand(atoi(optarg));
				break;
			case 'i':
				iflag = 1;
				filein = optarg;
				input = fopen(filein, "r");
				break;
			case 'o':
				oflag = 1;
				fileout = optarg;
				output = fopen(fileout,"w");
				break;
			case 'c':
				cflag = 1;
				countout = fopen(optarg,"w");

				break;
			case '?':
				//print usage string
		//		print the usage string to stderr
				fprintf(stderr,"unknown flag -%c\n",optopt); 
				exit(0);
				break;
			case ':':
			  	//print the usage string
				fprintf(stderr,"missing argument");
				exit(0);
				break;
		}

	struct timeval start_time;
	getTime(&start_time);

	//-u print usage string
	if(uflag){
	//	fprintf(stderr, "error out with fprintf -u flag\n");
		fprintf(stderr, "lab1 [-u] [-g] [-n <num-integers>] [-m <min-int>] [-M <max-int>] [-s <seed>]\n[-i <input-file-name>] [-o <output-file-name>] [-c <count-file-name>]\n");
		//perror("error out with perror\n");
		exit(1);
	}
	if(max > 1000000){
		fprintf(stderr, "Error max value of 10000000");
		exit(1);
	} 
	if(min >= max){
		fprintf(stderr, "Error max must be larger than min");
		exit(1);
	}

	// read specified but input failed to open
	if(iflag && input == NULL){
		//strcat path + filename? 
		char errorMsg[80] = "File failed to open ";
		strcat(errorMsg,filein);
		strcat(errorMsg,"\nError\n");
		fprintf(stderr,errorMsg);
		exit(1);
	}

	// write specified but output failed to open
	if(oflag && output == NULL){
		char errorMsg[80] = "File failed to open ";
		strcat(errorMsg,fileout);
		strcat(errorMsg,"\nError\n");
		fprintf(stderr,errorMsg);
		exit(1);	
	}
	if(cflag && countout == NULL){
		fprintf(stderr,"File failed to open\n");
		exit(1);
	}
	
	//read from a file
	if(!gflag){
		if(iflag){
			int num =0;
			if(nflag){
				expected_num = numInts; 
			}
			else{
				fscanf(input, "%d", &num);
				expected_num = num;
			}
			numInts = 0;
			//allocate memory
			input_numbers = (int *)malloc(expected_num * sizeof(int)); 
			//failed to allocate memory
			if(!input_numbers){
				fprintf(stderr,"could not allocate memory\n");
				exit(0);
			}
			//read in integers 
			while(fscanf(input, "%d",&num ) == 1 && numInts < expected_num){
								//check numbers fall between min and max
				if(num < min || num > max){
					fprintf(stderr, "%d does not fall in the range [%d,%d]\n",num,min,max);
					exit(0);
				}
				//set allocated memory to value from file
				*(input_numbers+numInts) = num;
				numInts++;	

			}
			//expected # != recieved.	
			//if(numInts != expected_num){
			//	fprintf(stderr, "did not recieve expected number of elements\n");
			//	exit(0);
			//}
		}
		else{ //input from command line? 
			int num = 0 ;
			if(nflag){
				expected_num = numInts;		
			}
			else{
				fscanf(stdin, "%d", &num);
				expected_num = num;
			}
				numInts = 0;
			//allocate memory
			input_numbers = (int *)malloc(expected_num * sizeof(int)); 
			//failed to allocate memory
			if(!input_numbers){
				fprintf(stderr,"could not allocate memory\n");
				exit(0);
			}
			else{
				int i = 0;
				while(fscanf(stdin, "%d",&num) ==1 && numInts < expected_num){
					//check numbers fall between min and max
					if(num < min || num > max){
						fprintf(stderr, "%d does not fall in the range [%d,%d]\n",num,min,max);
						exit(0);
					}
					//set allocated memory to value from file
					*(input_numbers+numInts) = num;
					numInts++;	
					if(numInts >= expected_num){
						break;
					}
					
				}
			}
		}
	}

	//allocate memory for array of matches
	match_count = (int*)malloc(26 * sizeof(int));
	if(!match_count){
		fprintf(stderr,"could not allocate memory\n");
		exit(0);
	}
	else{
		int i = 0;
		while(i < 26){
			*(match_count+i) = 0;
			i++;
		}
	}

	//get time
	//int start_time = gettimeofday();
	struct timeval finish_time;

	//generate mode 
	if(gflag){
		if(iflag){
			//flag the problem? //print usage string 
			fprintf(stderr,"cannot use input while in generate mode\n");
			exit(0);
		}

		//generate numInts integers between min and max. 
		if(oflag)
			fprintf(output,"%d\n",numInts);
		else
			fprintf(stdout,"%d\n",numInts);
		int i= 0;
		while(i < numInts){
			random = rand() % max; 
			if(random >= min){ 	
				i++;
				if(oflag){
					fprintf(output,"%d\n",random);
				}else
					printf("%d\n",random);
			}
		}
	}
	else{//sort mode !g 
		qsort(input_numbers, numInts, sizeof(int), compare); 
		int i = 0;
		int num = 0;		

		//count numbers that match user name
		while(i < numInts){
			num = *(input_numbers+i);
			if(strchr(user, (char)(num)) != NULL)
				if(num >= 97 && num <= 122)
					*(match_count + (num-97)) +=1;
			i++;
		}

		i = 0;
		//print to file
		if(oflag){
			while(i < numInts){
				fprintf(output,"%d\n",*(input_numbers+i));
				i++;
			}
		}else{//print to stdout
			while(i < numInts){
				fprintf(stdout,"%d\n",*(input_numbers+i));
				i++;
			}
		}
		if(cflag){
			i = 0;
			while(i < 26){
				if(*(match_count+i) > 0 ){
					num = 97 + i;
					fprintf(countout, "%c %d %d\n",(char)num, num,*(match_count+i) ); 
				}
				i++;
			}
		}
		else{
			i = 0;
			while(i < 26){
				if(*(match_count+i) > 0 ){
					num = 97 + i;
					fprintf(stdout, "%c %d %d\n",(char)num, num,*(match_count+i) ); 
				}
				i++;
			}
		}


	}

	//fprintf(stderr,"User: %s\n",getenv("USER"));
	//fprintf(stderr,"min = %d, max = %d\n", min, max);
	//fprintf(stderr,"input filename = %s\n",filein);
	//fprintf(stderr,"output filename = %s\n",fileout);

	//stop time timer 
	if(!gflag){
		getTime(&finish_time);
		int mtime = 1;
		int stime = 1;
		stime = finish_time.tv_sec - start_time.tv_sec  ;
		mtime = finish_time.tv_usec - start_time.tv_usec  ;
		fprintf(stderr, "elapsed program time : %d.** microseconds, %d seconds\n\n",mtime, stime);
	}

	//close files // these can be closed when finish. at least input can
	if(iflag && input != NULL)
		fclose(input);
	if(oflag && output != NULL)
		fclose(output);

	//free memory
	if(input_numbers != NULL)
		free(input_numbers);
	if(match_count != NULL)
		free(match_count);

	//successfull end of program. 
	exit(0);
}
