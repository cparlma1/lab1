all: lab2

lab1: lab1.c timething.h timething.c
	gcc -g -o lab1 timething.c lab1.c 

lab2: Lab1Parlman_cparlma1.c timething.h timething.c
	gcc -g -o lab1 timething.c Lab1Parlman_cparlma1.c

test:
	./lab1 -i output.txt -o output.txt -n 100 -c cout.txt

clean:
	rm -f lab1 output.txt cout.txt
